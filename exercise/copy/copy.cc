// copy a file line by line

/*
 * Copyright (c) 2004-2021 Detlef Vollmann, vollmann engineering gmbh
 * 
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <string>
#include <iostream>
#include <fstream>
#include <future>
#include <chrono>
#include <thread>

#include "timeHelper.hh"

using std::string;
using std::ifstream;
using std::ofstream;
using std::getline;
using std::async;
using std::future;
using std::launch;
using std::this_thread::yield;
using std::this_thread::sleep_for;
using std::chrono::nanoseconds;

class Copy
{
public:
    Copy()
      : infile("in.txt")
      , outfile("out.txt")
      , state(empty)
    {
    }

    void run();

private:
    enum DataState { empty, full, done };

    void read();
    void write();

    void doYield();

    std::ifstream infile;
    std::ofstream outfile;
    std::string line;
    DataState state;
};

void Copy::run()
{
    future<void> fRead{async(launch::async, [this] { read(); })};
    future<void> fWrite{async(launch::async, [this] { write(); })};

    fRead.get();
    fWrite.get();
}

void Copy::read()
{
    while (true)
    {
        while (state != empty)
        {
            doYield();
        }

        if (!getline(infile, line))
        {
            state = done;
            break;
        }
        else
        {
            state = full;
        }
    }
}

void Copy::write()
{
    while (state != done)
    {
        while (state != full)
        {
            doYield();
        }

        outfile << line << '\n';
        state = empty;
    }
}

void Copy::doYield()
{
    yield();
    //sleep_for(nanoseconds(1));
}

int main()
{
    Copy cp;

    Timer t;
    cp.run();
    std::cout << "Copy finished after " << t << " microseconds\n";

    return 0;
}
