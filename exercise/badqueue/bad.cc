// finding words in files
/*
 * Copyright (c) 2004-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "lqueue.hh"
#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <array>
#include <atomic>
#include <cstdlib>

#define USE_LOG 0
#include "log-helper.hh"

namespace
{
using namespace std::literals;

std::array textSample = {
    "Some random words"s,
    "void printCont(Cont const &c, std::ostream &os = cout);"s,
    "catch (system_error &err) { cerr << err.code().message() << endl; }"s,
    "cout << Minimum:  << mm.first << , Maximum:  << mm.second << '\n';"s,
    "void Lazy::createInstance() const"s,
    "auto objPrint = [&] { std::cout << obj << '\n'; };"s,
};

typedef LQueue<size_t> TextList;

TextList textQ;
std::string sWord = "const"s;
constexpr auto baseDelayTime = 1ms;
constexpr int loopCount = 10;//'000'000;
std::atomic<int> foundCount = 0;
std::atomic<int> realLoopCount = 0;

constexpr void delay(int cnt)
{
    if (cnt != 0)
    {
        std::this_thread::sleep_for(cnt*baseDelayTime);
    }
}

void searchWord(int id)
{
    LVAR(id);
    textQ.wait();
    LVAR(id);
    while(!textQ.empty())
    {
        LVAR(id);
        delay(0);
        ++realLoopCount;
        size_t textIdx = textQ.pop();
        std::string s = textSample[textIdx];

        if (s.find(sWord) != s.npos)
        {
            std::cout << "found in " << textIdx << '\n';
            ++foundCount;
        }
    }
}

void getText()
{
    LOG;
    for (int i = 0; i != loopCount; ++i)
    {
        LOG;
        size_t idx = rand() % textSample.size();
        textQ.push(idx);
        delay(0);
        textQ.notify();
        delay(0);
    }
}
} // unnamed namespace


int
main()
{
    srand(0);
    std::thread a(getText);
    std::thread b1(searchWord, 2);
    std::thread b2(searchWord, 3);

    a.join();
    b1.join();
    b2.join();

    std::cout << "total searched: " << realLoopCount
              << ", total found: " << foundCount << '\n';
	
    return 0;
}
