/*
 * Copyright (c) 2014-2020 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include "lest.hpp"

using std::cout;

typedef std::vector<int> IntVect;

namespace exercise
{
size_t countNums(IntVect const &v, int num)
{
    return std::count(v.begin(), v.end(), num) ;
}

auto minmax(IntVect const &v);

int sum(IntVect const &v);

double avg(IntVect const &v);

void delOdd(IntVect &v);

std::set<int> asSet(IntVect const &v);

void delEven(std::set<int> &si);

template <typename Cont>
void printCont(Cont const &c, std::ostream &os = cout)
{
    for (auto v: c)
    {
        os << v << ", ";
    }
    os << '\n';
}

IntVect testVect{1, 2, 2, 3, 3, -9};
const lest::test myTests[] =
{
    CASE("Test countNums w/ existing value")
    {
        size_t result = countNums(testVect, 2);
        EXPECT(result == 2u);
    },
};
} // namespace exercise

//#define HAVE_MINMAX 1
//#define HAVE_SUM 1
//#define HAVE_AVG 1
//#define HAVE_DEL_ODD 1
//#define HAVE_AS_SET 1
//#define HAVE_DEL_EVEN 1

#include "test-stdlib.hh"

int main()
{
    using namespace exercise;

    int status = lest::run(myTests, exerciseTest::lestArgs, std::cerr);
    status += lest::run(exerciseTest::libTests, exerciseTest::lestArgs, std::cerr);

    std::clog << status << " test suites failed.\n";

    IntVect vi{ 22, 33, 44, 12, 2, 18, 4, 27, 44, 91 };

    cout << "Original vector: ";
    printCont(vi);

#ifdef HAVE_MINMAX
    auto mm = minmax(vi);
    cout << "Minimum: " << mm.first << ", Maximum: " << mm.second << '\n';
#endif
#ifdef HAVE_SUM
    cout << "Total: " << sum(vi) << '\n';
#endif
#ifdef HAVE_AVG
    cout << "Average: " << avg(vi) << '\n';
#endif
#ifdef HAVE_AS_SET
    std::set<int> si = asSet(vi);
#endif
#if defined(HAVE_DEL_ODD)
    delOdd(vi);
    cout << "Vector without odd numbers: ";
    printCont(vi);
#endif
#if defined(HAVE_AS_SET)
    cout << "Original set:\n";
    printCont(si);
#endif
#if defined(HAVE_DEL_EVEN)
    delEven(si);
    cout << "Set without even numbers: ";
    printCont(si);
#endif

    return 0;
}
