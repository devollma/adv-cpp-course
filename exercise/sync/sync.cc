/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <iostream>
#include <future>

namespace exercise
{
class Lazy
{
public:
    Lazy() = default;
    void print(std::ostream &o) const;

private:
    void createInstance() const;

    mutable int *pImpl = nullptr;
};

inline void Lazy::print(std::ostream &o) const
{
    createInstance();
    o << *pImpl;
}

inline void Lazy::createInstance() const
{
    if (!pImpl)
    {
        pImpl = new int(42);
    }
}

inline std::ostream &operator<<(std::ostream &o, Lazy const &obj)
{
    obj.print(o);
    return o;
}
} // namespace exercise

int main()
{
    using namespace exercise;
    using std::async;

    Lazy obj;
    auto objPrint = [&] { std::cout << obj << '\n'; };

    auto f1 = async(objPrint);
    auto f2 = async(objPrint);

    f1.get();
    f2.get();

    return 0;
}
