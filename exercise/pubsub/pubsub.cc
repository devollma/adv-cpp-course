// a simple publish/subscribe broker

/*
 * Copyright (c) 2021 Detlef Vollmann, vollmann engineering gmbh
 * 
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <string>
#include <bitset>
#include <iostream>
#include <thread>
#include <chrono>
#include <future>
#include <vector>

namespace
{
class Broker
{
public:
    Broker()
    {
    }

    void publish(std::string info)
    {
        wait([this] { return curCount == 0; });
        data = std::move(info);
        curCount = subscribeCount;
        subscriberSet.reset();
    }

    size_t subscribe()
    {
        if (curCount != 0 || done)
        {
            return 0;
        }

        ++subscribeCount;
        return subscribeCount;
    }

    std::string get(size_t subId)
    {
        if (subId == 0 || subId > subscribeCount)
        {
            return {};
        }

        wait([this, subId]
             { return ((curCount != 0 && !subscriberSet[subId - 1])
                       || done); });

        // we don't want to wait in finish(), so we need to check here
        if (curCount != 0 && !subscriberSet[subId - 1])
        {
            --curCount;
            subscriberSet[subId - 1] = true;

            return data;
        }

        return {};
    }

    void finish()
    {
        done = true;
    }

private:
    template <class F>
    void wait(F pred)
    {
        while (!pred())
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }

    static constexpr size_t maxSubscribers = 64;
    std::string data;
    size_t subscribeCount = 0;
    size_t curCount = 0;
    std::bitset<maxSubscribers> subscriberSet;
    bool done = false;
};

void subscriber(Broker &br)
{
    size_t myId = br.subscribe();

    for (std::string info = br.get(myId); !info.empty(); info = br.get(myId))
    {
        std::cout << myId << ": " << info << '\n';
    }

    std::cout << myId << "done\n";
}
} // unnamed namespace

int main()
{
    std::vector<std::future<void>> waiters;

    Broker testBroker;

    // start some subscribers
    for (int i = 0; i != 5; ++i)
    {
        waiters.push_back(std::async(std::launch::async,
                                     [&testBroker]
                                     {
                                         subscriber(testBroker);
                                     }));
    }

    // publish some data
    testBroker.publish("First");
    // and immediately again
    testBroker.publish("Second");

    // wait a bit before the next
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    testBroker.publish("Third");

    // stop the broker
    testBroker.finish();

    std::cout << "Publish finished\n";

    // destructor of waiters waits for all asyncs

    return 0;
}
