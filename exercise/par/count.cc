/*
 * Copyright (c) 2014-2020 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <fstream>
#include <iostream>
#include <string>

#include <algorithm>
#include <execution>

#include "timeHelper.hh"


unsigned int target{0};

unsigned int mangle1(char c)
{
    unsigned int v = c;
    v = ~v;
    v = v << 1;
    v |= 0x1201u;
    v /= 2;
    return v;
}

unsigned int mangle2(char c)
{
    for (int i = 0; i != 100; ++i)
    {
        unsigned int v{mangle1(c)};
        c = v & 0xff;
    }
    return mangle1(c);
}

std::string setup()
{
    constexpr size_t txtCap{1'000'000};
    std::string txt;
    txt.reserve(txtCap);

    std::string fTxt;
    size_t fSize;
    std::ifstream in("carol11ms.txt", std::ios::in | std::ios::binary);
    if (in)
    {
        in.seekg(0, std::ios::end);
        fSize = in.tellg();
        fTxt.resize(fSize);
        in.seekg(0, std::ios::beg);
        in.read(&fTxt[0], fSize);
        in.close();
    } else {
        throw std::runtime_error{"Couldn't open file"};
    }

    size_t rem{txtCap};
    while (rem > 0)
    {
        txt.append(fTxt, 0, rem);
        rem = txt.capacity() - txt.size();
    }

    target = mangle2('e');

    return txt;
}

bool check(char c)
{
    return mangle2(c) == target;
}

int main()
{
    std::string txt{setup()};
    auto testVal = mangle2('e');

    namespace par = std::execution;

    std::cout << "Starting...\n";
    Timer t;

    long eCount = 0;

    // counting 'e's directly
    for (char c: txt)
    {
        if (c == 'e')
        {
            ++eCount;
        }
    }
    std::cout << "unmangled: Size: " << txt.size() << ", Number of e: " << eCount;
    std::cout << ", Time(us): " << t << '\n';
    t.reset();

    // giving more work by hashing
    eCount = 0;
    for (char c: txt)
    {
        if (mangle2(c) == testVal)
        {
            ++eCount;
        }
    }
    std::cout << "normal:    Size: " << txt.size() << ", Number of e: " << eCount;
    std::cout << ", Time(us): " << t << '\n';

    return 0;
}
