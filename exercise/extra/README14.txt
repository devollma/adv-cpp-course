Exercise "Finding Bugs"
base: badqueue

This is an example originally searching words in files
(simplified grep).
It's simplified here to search words in an array of strings.
It's fullof concurrency problems.

 - Try to find them by code review.

 - Try to find them by running with thread sanitizer.
   This requires to at least work around some of the problems.
