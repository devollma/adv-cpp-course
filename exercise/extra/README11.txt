Exercise "Mutex Synchronization"

 - Part 1 (sync): Lazy concurrent init
   This is a sample class that uses lazy evaluation.
   If accessed from different threads, this causes data races
   (thread sanitizer tells you about them).

   - Fix the data races with a mutex.

 - Part 2 (copy): Copy file
   This is a (silly) implementation of multi-threaded file copy.
   One thread does the reading, another the writing.
   - fix race conditions using a (single) mutex
   - fix any "deadlocks"

 - Part 3 (max): Use global state for 'max'
   - min/max variables
   - synchronize properly
   - adjust size for timings down!

