Exercise "Condition Variable"

 - Part 1: Copy
   - instead of busy waiting use a condition variable

 - Part 2: Publish/Subscribe
   - synchronize properly
   - use a condition variable to signal available data / free data and end
