Exercise "Warm Up / Range based for"

 - Look where auto and range based for loops make sense.
 - Use member default initialization where appropriate.
 - Try out unified initialization.
