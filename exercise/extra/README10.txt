Exercise "Parallel Algorithms"

 - Part 1: Base: max
   - Parallelize 'max' using parallel algorithm
   - based on original version
   - there's no parallelized iota(), simply use the basic version
     - in header <numeric>
   - for random values use for_each()
   - for min/max use minmax_element (returns pair of iterators!)
   - timing statistics vs. single-threaded version and own parallel version

 - Part 2: Base: par/count.cc
   - Try to make the counting faster by parallelizing it.
   - Comparing the character w/ mangle2() is deliberately slow
     to make it a computing task and not a memory task.
