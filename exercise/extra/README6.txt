Exercise "Library"

 - new project based on 'stdlib'

 - Write functions that return for the given vector
   - minmax(vect): minimum and maximum value
   - sum(vect): the total sum
   - avg(vect): the average value

 - Write functions that do for the given vector
   - delOdd(vect): removes the odd numbers
   - asSet(vect): copies the values into a set and returns the set

 - Write a function delEven(set) that removes the even numbers.
