Exercise "Class template"

 - Make PathShape a template over PathBase.
   Hold now the 'path' by value.
 - Adjust calls in demo.cc accordingly.
 - Implementation in header.
