Exercise "Queue"

 - Part 1: Copy
   - simplify the copy by using a queue
   - reader pushes, writer pops

 - Part 2: Publish/Subscribe
   - again simplify by using a queue
   - a list of queues, one for each subscriber
   - publish() simply pushes in all the queues
   - get() simply pops form the queue
   - mutex for subscribe() to protect list of queues is still needed
