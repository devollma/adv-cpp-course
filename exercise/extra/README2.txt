Exercise "Function Template"
func-templ, non-template function given

 - Given function readInt() reads an integer from a stream
   and checks it against given bounds.
   Repeats when out of bounds.
   Returns false if non-number is entered or end of file.
   Make it a function template readVal() that works for ints as before
   but also works for all kinds of numbers.
   Adapt main() accordingly.

 - Make printVector() a function template printCont() that works with
   all kind of containers and values.
