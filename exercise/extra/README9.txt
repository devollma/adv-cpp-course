Exercise "Basic Threads"

 - Part 1: Alarms
   - start each alarm in it's own thread (using async)
   - keep a list of the futures

 - Part 2: Parallelize 'max'
   - fill and min/max
   - split the vector
   - start one async per slice
   - wait for all to finish
   - look at timing statistics vs. single-threaded version
   - possibly adjust size for timings
   - for timings always use 'release' build
