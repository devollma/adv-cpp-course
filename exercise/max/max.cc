// find maximum in a vector
/*
 * Copyright (c) 2021 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <utility>

#include "timeHelper.hh"


namespace
{
constexpr size_t elemCount = 30'000'000;
std::vector<int> nums(elemCount);

static_assert(sizeof(int) == sizeof(int32_t), "Wrong int size");

// Marsaglia's algorithm
int32_t xorshift32Val(uint32_t in)
{
    in ^= in << 13;
    in ^= in >> 17;
    in ^= in << 5;
    return in;
}

void fillVect()
{
    for (size_t i = 0; i != elemCount; ++i)
    {
        nums[i] = i + 1;
    }

    for (size_t i = 0; i != elemCount; ++i)
    {
        nums[i] = xorshift32Val(nums[i]);
    }
}

auto findMinMax()
{
    int min = nums[0];
    int max = nums[0];

    for (size_t i = 1; i != elemCount; ++i)
    {
        if (nums[i] > max)
        {
            max = nums[i];
        }
        if (nums[i] < min)
        {
            min = nums[i];
        }
    }
    return std::make_pair(min, max);
}
} // unnamed namespace


int main()
{
    Timer t;
    // warm the memory
    fillVect();
    std::cout << "First fill time: " << t << '\n';

    t.reset();
    fillVect();
    std::cout << "Fill time: " << t << '\n';

    t.reset();
    auto minMax = findMinMax();
    std::cout << "Max time: " << t
              << ", min: " << minMax.first
              << ", max: " << minMax.second
              << '\n';

    return 0;
}
