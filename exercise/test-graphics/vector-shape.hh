/*
 * Copyright (c) 2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef VECTOR_SHAPE_HH_SEEN_
#define VECTOR_SHAPE_HH_SEEN_

#include <vector>
#include <memory>
#include <initializer_list>
#include <memory_resource>
#include <array>
#include <stdexcept>

namespace exercise
{
class Shape;
} // namespace exercise

namespace std
{
namespace impl
{
// we can't put the allocator inside the vector, as the allocator
// would be destructed before the base vector would be destructed,
// leaving a dangling pure virtual function for do_deallocate...
// so we put a pool of such allocators here
// the memory is still inside the vector
// we never call the destructor of the monotonic_buffer_resources,
// but that should be fine (they don't hold any resources anyway)
inline pmr::monotonic_buffer_resource *nextMbr(void *pool, size_t size)
{
    typedef pmr::monotonic_buffer_resource MBR;
    typedef array<byte, sizeof(MBR)> mbrBufT;
    static array<mbrBufT, 50> mbrPool;
    static size_t curMbr = 0;

    if (curMbr == 50)
    {
        throw bad_alloc();
    }

    new (&mbrPool[curMbr]) MBR(pool, size, pmr::null_memory_resource());
    ++curMbr;

    return reinterpret_cast<MBR *>(&(mbrPool[curMbr-1]));
}

// however, as we want to swap vectors, we need one single big pool...
inline pmr::monotonic_buffer_resource *shapeMbr()
{
    static array<byte, 4096*sizeof(void *)> buf;
    static pmr::monotonic_buffer_resource pool(buf.data(), buf.size(),
                                               pmr::null_memory_resource());
    return &pool;
}

} // namespace impl

template <>
class vector<exercise::Shape *, std::allocator<exercise::Shape *>>
    : public pmr::vector<exercise::Shape *>
{
    typedef exercise::Shape Shape;
    typedef pmr::vector<Shape *> Base;
public:
    vector()
      : Base(impl::shapeMbr())
    {}
    vector(vector const &other)
      : Base(other, impl::shapeMbr())
    {}
    // this is special: we want copy instead of move
    vector(vector &&other)
      : Base(other, impl::shapeMbr())
    {}

    ~vector() = default;

    vector& operator=(vector const &) = default;
    vector& operator=(vector&& rhs) noexcept
    {
        assign(rhs.begin(), rhs.end());
        return *this;
    }

private:
#if 0
    //array<byte, 20*sizeof(void *)> buf;
    struct FixedMemAlloc : pmr::memory_resource
    {
        FixedMemAlloc(void *pool, size_t size)
          : buf(pool)
          , avail(size)
        {}
        void* do_allocate(size_t n, size_t alignment) override
        {
            void *p = align(alignment, n, buf, avail);
            buf = static_cast<byte *>(buf) + n;
            avail -= n;

            return p;
        }
        void do_deallocate(void*, size_t, size_t) override
        {
        }
        bool do_is_equal(const memory_resource& other) const noexcept override
        {
            return &other == this;
        }

        void *buf = nullptr;
        size_t avail = 0;
    };
    struct NoMemAlloc : pmr::memory_resource
    {
        void* do_allocate(size_t, size_t) override
        {
            throw bad_alloc();
        }
        void do_deallocate(void*, size_t, size_t) override
        {
        }
        bool do_is_equal(const memory_resource& other) const noexcept override
        {
            return &other == this;
        }
    };
    //pmr::monotonic_buffer_resource alloc{buf.data(), buf.size(), pmr::null_memory_resource()};
    //NoMemAlloc up;
    //pmr::monotonic_buffer_resource alloc{buf.data(), buf.size(), &up};
    //FixedMemAlloc alloc{buf.data(), buf.size()};
#endif

};
} // namespace std
#endif /* VECTOR_SHAPE_HH_SEEN_ */
