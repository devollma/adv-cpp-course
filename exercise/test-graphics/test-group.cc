/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <group.hh>

#include "lest.hpp"
#include "allocmock.hh"
#include "test-classes.hh"

#include <type_traits>
#include <typeinfo>
#include <utility>


/*
 * testing group move
 *
 * Group constructor and destructor don't do any external
 * actions (except new/delete).
 * So we need to provide a mock derived from Group to track these.
 * This is fine as Group is designed for derivation.
 * This doesn't help.
 * What we need to check are operations on elements.
 * One option is to mock Shape for Group test only (providing a mock header).
 * Another option is to provide our own new/delete and track these.
 * Our new/delete provide mocks for trackNew/trackDelete.
 */

namespace
{
using namespace exercise;
using namespace exerciseTest;

struct GroupDeriv : public Group
{
    GroupDeriv() = default;
    GroupDeriv(bool *dtorMarker)
      : Group{}
      , dtorFlag{dtorMarker}
    {
    }

#ifdef HAS_GROUP_MOVE_CTOR
    // we want to check the destruction of the original object
    // so our move operations only move the base
    GroupDeriv(GroupDeriv &&other)
      : Group{std::move(other)}
    {}
#endif

    ~GroupDeriv()
    {
        if (dtorFlag)
        {
            *dtorFlag = true;
        }
    }

#ifdef HAS_GROUP_MOVE_ASSIGNMENT
    GroupDeriv &operator=(GroupDeriv &&rhs)
    {
        Group::operator=(std::move(rhs));
        return *this;
    }
#endif

    bool *dtorFlag = nullptr;
};

// looks like for CentOS devtoolset-8 std::string doesn't call our new
// it actually looks like COW optimization
// old ABI on CentOS 7
// possibly test _GLIBCXX_USE_DUAL_ABI and _GLIBCXX_USE_CXX11_ABI
std::string longString = "A pretty long string that should be long enough to not fit into any small string optimization";

[[maybe_unused]] bool operator==(Position p1, Position p2)
{
    return p1.x == p2.x && p1.y == p2.y;
}

// lest throws an exception on failure, which leaves the test case
// even before the message is printed
// if due to the failed test the destructor causes trouble, we have a problem
// so we keep a global group here for such tests
// and we deliberately leak it
Group *leakedGroup = new Group(longString);
Group &globalTestGroup = *leakedGroup;

// for move assignment we need a second such group (empty name)
Group *leakedGroupEmpty = new Group;
Group &globalTestGroupEmpty = *leakedGroupEmpty;

#if 0
#ifdef HAVE_PATH_SHAPE_TEMPL
#ifdef HAVE_PATH_SHAPE_TEMPL_BOOL
#define FRAME_INIT new PathShape<Rectangle, false>{Rectangle{70, 30}, "F1"}
typedef PathShape<Rectangle, false> *FrameType;
#else
typedef PathShape<Rectangle> *FrameType;
#define FRAME_INIT new PathShape<Rectangle>{Rectangle{70, 30}, "F1", false}
#endif // HAVE_PATH_SHAPE_TEMPL_BOOL
#else
typedef PathShape *FrameType;
#define FRAME_INIT new PathShape{new Rectangle{70, 30}, "F1", false}
#endif // HAVE_PATH_SHAPE_TEMPL
#endif // 0

struct TestGroup
{
    TestGroup()
      : s1{new TestShape{"S1", Position{20, 20}}}
      , s2{new TestShape{"S2", Position{150, 70}}}
      , s3{new TestShape{"S3", Position{1150, 700}}}
      , g{longString}
    {
        g.addChild(s1);
        g.addChild(s2);
        g.move(10, 10);
    }

    ~TestGroup()
    {
        delete s3;
        // s1 and s2 are cleaned up by g's dtor
    }

    TestShape *s1;
    TestShape *s2;
    TestShape *s3;
    Group g;
};

#if 0 // this check doesn't work for now
static_assert(!std::is_copy_constructible<Group>::value, "Group must not be copy constructible");
static_assert(!std::is_copy_assignable<Group>::value, "Group must not be copy assignable");
#endif

#ifdef HAS_GROUP_MOVE_CTOR
static_assert(std::is_move_constructible<Group>::value, "Group must be move constructible");
#endif

#ifdef HAS_GROUP_MOVE_ASSIGNMENT
static_assert(std::is_move_assignable<Group>::value, "Group must be move assignable");
#endif


using trompeloeil::_;

extern const lest::test groupTests[] =
{
    {CASE("Group ctor/dtor memory calls must match")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management
             bool shape1Destruction = false;
             bool shape2Destruction = false;
             {
                 ALLOW_CALL(tracer, trackNew(_, _));
                 ALLOW_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 TestGroup gTest;
                 gTest.s1->destructionMarker = &shape1Destruction;
                 gTest.s2->destructionMarker = &shape2Destruction;
             }
             EXPECT(shape1Destruction == true);
             EXPECT(shape2Destruction == true);
             EXPECT(testOk());
             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},

#ifdef HAS_GROUP_MOVE_CTOR
    {CASE("Shape move ctor must move (not copy)")
     // while this test case is not really portable due to SSO,
     // in practice it works
     {
         TestShape movedFrom{longString};

         TestShape movedTo{std::move(movedFrom)};

         EXPECT(movedFrom.getName().empty() == true);
         EXPECT(movedTo.getName() == longString);
     }},
    {CASE("Shape move ctor must not cause any memory calls")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management

             TestShape sTest{longString+"X"};
             EXPECT(testAlloc.getCount() >= 0);

             TestShape s2;
             s2.~TestShape();

             {
                 FORBID_CALL(tracer, trackNew(_, _));
                 FORBID_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 new (&s2) TestShape{std::move(sTest)};
             }
             EXPECT(testOk());
             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},

    {CASE("Group move ctor must move (not copy) base class")
     {
         Position testPos{10, -44.33};
         Group oldGroup{longString};
         oldGroup.setPosition(testPos);

         Group newGroup;
         newGroup.~Group();

         new (&newGroup) Group{std::move(oldGroup)};

         EXPECT(newGroup.getPosition() == testPos);
         EXPECT(newGroup.getName() == longString);
         // again not portable due to SSO, but works
         EXPECT(oldGroup.getName().empty() == true);
     }},

    {CASE("Group move ctor must move (not copy) children (moved-from must be empty)")
     {
         std::array<int, 5> seqBuf{0};
         Sequencer<int> seq{&(seqBuf[0]), seqBuf.size()};
         globalTestGroup.addChild(new TestShape{1, &seq});
         globalTestGroup.addChild(new TestShape{2, &seq});

         Group newGroup;
         newGroup.~Group();

         new (&newGroup) Group{std::move(globalTestGroup)};

         // old children should be empty, so draw() does nothing
         std::array<int, 5> oldDrawSequence{0, 0, 0, 0, 0};
         globalTestGroup.draw(nullptr);
         EXPECT(seqBuf == oldDrawSequence);

         // new children should have shapes now, so draw() draws children
         std::array<int, 5> newDrawSequence{1, 2, 0, 0, 0};
         newGroup.draw(nullptr);
         EXPECT(seqBuf == newDrawSequence);

         // now it should be safe to destruct our test group
         // and reset it to a safe state
         globalTestGroup.~Group();
         new (&globalTestGroup) Group(longString);
     }},

    {CASE("Group move ctor must not cause any memory calls")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management
             globalTestGroupEmpty.setName(longString);
             globalTestGroupEmpty.addChild(new TestShape{longString});
             globalTestGroupEmpty.addChild(new TestShape{"S2"});

             Group g2;
             g2.~Group();

             {
                 FORBID_CALL(tracer, trackNew(_, _));
                 FORBID_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 new (&g2) Group{std::move(globalTestGroupEmpty)};
             }
             EXPECT(testOk());
             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);

         // now it should be safe to destruct our test group
         // and reset it to a safe state
         globalTestGroupEmpty.~Group();
         new (&globalTestGroupEmpty) Group;
     }},

    {CASE("Group move ctor must move ownership (no deallocation from moved from destructor)")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         {
             TestGroup gTest;

             Group g2;
             g2.~Group();

             {
                 FORBID_CALL(tracer, trackNew(_, _)); // !!! doesn't work w/ apply in ~Group
                 FORBID_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 new (&g2) Group{std::move(gTest.g)};
                 gTest.g.~Group(); // apply() may cause new!!!
             }
             EXPECT(testOk());

             new (&gTest.g) Group{}; // may cause new
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},
#endif // HAS_GROUP_MOVE_CTOR

#ifdef HAS_GROUP_MOVE_ASSIGNMENT
    {CASE("Shape move assignment must move (not copy)")
     {
         TestShape movedFrom{longString};

         TestShape movedTo;
         movedTo = std::move(movedFrom);

         EXPECT(movedFrom.getName().empty() == true);
         // again not portable due to SSO, but works
         EXPECT(movedTo.getName() == longString);
     }},
    {CASE("Shape move assignment w/ empty target must not cause any memory calls")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         {
             TestShape sTest{longString};
             EXPECT(testAlloc.getCount() >= 0);

             TestShape s2;
             s2.~TestShape();

             // default ctor may cause new
             {
                 ALLOW_CALL(tracer, trackNew(_, _));
                 FORBID_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 new (&s2) TestShape{};
             }
             {
                 FORBID_CALL(tracer, trackNew(_, _));
                 ALLOW_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 s2 = std::move(sTest);
             }
             EXPECT(testOk());
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},

    {CASE("Group move assignment must not use dtor/ctor impl")
     {
         GroupDeriv oldGroup;

         bool oldTargetGroupDestructed = false;
         GroupDeriv newGroup{&oldTargetGroupDestructed};
         Group *newGroupPtr = &newGroup;
         newGroup = std::move(oldGroup);

         EXPECT(typeid(*newGroupPtr) == typeid(GroupDeriv));
         EXPECT(oldTargetGroupDestructed == false);
     }},

    {CASE("Group move assignment must move base class")
     {
         Position testPos{10, -44.33};
         Group oldGroup{longString};
         oldGroup.setPosition(testPos);

         Group newGroup;
         newGroup = std::move(oldGroup);

         EXPECT(newGroup.getPosition() == testPos);
         EXPECT(newGroup.getName() == longString);
         // again not portable due to SSO, but works
         EXPECT(oldGroup.getName().empty() == true);
     }},

    {CASE("Group move assignment must move (not copy) children")
     {
         Group oldGroup{longString};
         std::array<int, 5> seqBuf{0};
         Sequencer<int> seq{&(seqBuf[0]), seqBuf.size()};
         globalTestGroup.addChild(new TestShape{1, &seq});
         globalTestGroup.addChild(new TestShape{2, &seq});

         Group newGroup;

         newGroup = std::move(globalTestGroup);

         // old children should be empty, so draw() does nothing
         std::array<int, 5> oldDrawSequence{0, 0, 0, 0, 0};
         globalTestGroup.draw(nullptr);
         EXPECT(seqBuf == oldDrawSequence);

         // new children should have shapes now, so draw() draws children
         std::array<int, 5> newDrawSequence{1, 2, 0, 0, 0};
         newGroup.draw(nullptr);
         EXPECT(seqBuf == newDrawSequence);

         // now it should be safe to destruct our test group
         // and reset it to a safe state
         globalTestGroup.~Group();
         new (&globalTestGroup) Group(longString);
     }},

    {CASE("Group move assignment must move ownership (correct destruction)")
     {
         bool movedFromDestructionS1 = false;
         auto s1 = new TestShape(longString);
         s1->destructionMarker = &movedFromDestructionS1;
         globalTestGroup.addChild(s1);
         bool movedFromDestructionS2 = false;
         auto s2 = new TestShape("S2");
         s2->destructionMarker = &movedFromDestructionS2;
         globalTestGroup.addChild(s2);

         //Group movedTo;
         globalTestGroupEmpty = std::move(globalTestGroup);

         // destructing movedFrom should not cause destruction of shapes
         globalTestGroup.~Group();
         EXPECT(movedFromDestructionS1 == false);
         EXPECT(movedFromDestructionS2 == false);

         // destructing movedTo should cause destruction of shapes
         globalTestGroupEmpty.~Group();
         // just for better messages we copy the bool
         bool movedToDestructionS1 = movedFromDestructionS1;
         bool movedToDestructionS2 = movedFromDestructionS2;
         EXPECT(movedToDestructionS1 == true);
         EXPECT(movedToDestructionS2 == true);

         // and reset global groups to default state
         // now we need to restore the objects for "normal" destruction
         new (&globalTestGroup) Group(longString);
         new (&globalTestGroupEmpty) Group;
     }},

    {CASE("Group move assignment w/ empty target must not cause any memory calls")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         {
             globalTestGroupEmpty.setName(longString);
             globalTestGroupEmpty.addChild(new TestShape{longString});
             globalTestGroupEmpty.addChild(new TestShape{"S2"});

             Group g2;
             g2.~Group();

             // default ctor may cause new
             {
                 ALLOW_CALL(tracer, trackNew(_, _));
                 FORBID_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 new (&g2) Group{};
             }
             {
                 FORBID_CALL(tracer, trackNew(_, _));
                 ALLOW_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 g2 = std::move(globalTestGroupEmpty);
             }
             EXPECT(testOk());
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
         // now only g2 was cleaned up, but globalTestGroupEmpty wasn't cleaned up yet
         // it should be empty due to the tests above
         // if we get a crash here, the moved-from globalTestGroupEmpty wasn't properly cleaned-up
         // by move assignment
         globalTestGroupEmpty.~Group();
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);

         // reset our test group to a safe state
         new (&globalTestGroupEmpty) Group;
     }},

    {CASE("Group move assignment must move ownership (no deallocation from moved from destruction)")
     {
         AllocTracerMock tracer;
         AllocMock testAlloc;
         {
             TestGroup gTest;
             Group g2;
             g2.~Group();

             // default ctor may cause new
             {
                 ALLOW_CALL(tracer, trackNew(_, _));
                 FORBID_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 new (&g2) Group{};
             }
             {
                 FORBID_CALL(tracer, trackNew(_, _));
                 ALLOW_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 g2 = std::move(gTest.g);
             }
             {
                 FORBID_CALL(tracer, trackNew(_, _));
                 FORBID_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 gTest.g.~Group();
             }
             {
                 ALLOW_CALL(tracer, trackNew(_, _));
                 FORBID_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 new (&gTest.g) Group{};
             }
         }
         EXPECT(testOk());
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},

    {CASE("Group move assignment must release old target (and not release anything twice)")
     {
         AllocMock testAlloc;
         int gTestMem = 0;
         {
             Group movedFrom;
             auto s1 = new TestShape{longString};
             movedFrom.addChild(s1);

             {
                 gTestMem = testAlloc.getCount();
                 Group g2;
                 TestShape *s{new TestShape{longString}};
                 bool sDestructed = false;
                 s->destructionMarker = &sDestructed;
                 g2.addChild(s);

                 g2 = std::move(movedFrom); // here old g2 should get cleaned up

                 EXPECT(sDestructed == true);
                 EXPECT(testAlloc.getCount() == gTestMem);
             }
             // we don't know exactly the count of TestShape allocs (possible SSO)
             EXPECT(testAlloc.getCount() < gTestMem);
             EXPECT(testAlloc.getCount() >= 0);
             // if we get a crash after this line, it's probably
             // because destructor of movedFrom still tries to clean up s1
         }
         EXPECT(testAlloc.ok());
         EXPECT(testAlloc.getCount() == 0);
     }},
#endif // HAS_GROUP_MOVE_ASSIGNMENT
#ifdef HAS_GROUP_GET_SHAPE
    {CASE("Check Group::getShape() for moved children")
     {
         Group g{"g"};
         auto t1 = new TestShape{"S1", Position{20, 20}};
         auto t2 = new TestShape{"S2", Position{2, 2}};

         g.addChild(t1);
         EXPECT(&(g.getShape("S1")->get()) == t1);
         EXPECT(g.getShape("S2").has_value() == false);

         *t1 = std::move(*t2);
         EXPECT(&(g.getShape("S2")->get()) == t1);
         EXPECT(g.getShape("S1").has_value() == false);
     }},
#endif // HAS_GROUP_GET_SHAPE
};
} // unnamed namespace

int main(int argc, char *argv[])
{
    exerciseTest::setTromploeilReporter();

    return lest::run(groupTests, argc, argv, std::cerr);
}
