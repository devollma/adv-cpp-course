/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef TEST_CLASSES_HH_SEEN_
#define TEST_CLASSES_HH_SEEN_

#include "trompeloeil.hpp"
#include "lest.hpp"

#include <shape.hh>

#include <stdexcept>
#include <initializer_list>
#include <set>

namespace exerciseTest
{
using exercise::Shape;
using exercise::Position;

template <typename T>
struct Sequencer
{
    Sequencer() = default;
    Sequencer(T *start, size_t count)
      : p{start}
      , end{p + count}
    {}

    void store(T val)
    {
        if (!p) return;

        if (p == end)
        {
            throw std::out_of_range{"sequence too long"};
        }

        *p = val;
        ++p;
    }

    T *p = nullptr;
    T *end = nullptr;
};
extern Sequencer<GuiFuncs> *globalGuiSeq;

enum GuiFuncs;

struct Forbidden : std::logic_error
{
    Forbidden(GuiFuncs fn)
      : std::logic_error("Forbidden Call")
      , funcName{fn}
    {}
    //const char *what() const noexcept override;
    void print(std::ostream &o) const;

    GuiFuncs funcName;
};

struct ForbiddenCall
{
    ForbiddenCall(std::initializer_list<GuiFuncs> const &);
    void call(GuiFuncs what);

    std::set<GuiFuncs> forbiddenFuncs;
    bool forbiddenFunctionCalled = false;
};
extern ForbiddenCall *globalForbidden;

struct TestShape : public Shape
{
    using Shape::Shape;
    TestShape(int i);
    TestShape(int i, Sequencer<int> *seq);
    TestShape(TestShape const &) = delete;
    TestShape(TestShape &&other);

    ~TestShape();

    TestShape &operator=(TestShape const &) = delete;
    TestShape &operator=(TestShape &&rhs);

    void doDraw(cairo_t *) const override;

    int id = 0;
    bool *destructionMarker = nullptr;
    Sequencer<int> *drawIdMarker = nullptr;
    Sequencer<GuiFuncs> *drawFuncMarker = nullptr;
};

class ShapeMock : public Shape
{
public:
    using Shape::Shape;

    MAKE_CONST_MOCK1(doDraw, void(cairo_t *));

};

bool testOk();
void setTromploeilReporter();
} // namespace exerciseTest
#endif /*  TEST_CLASSES_HH_SEEN_ */
