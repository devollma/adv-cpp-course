/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include "test-classes.hh"
#include "allocmock.hh"

#include "lest.hpp"

#include "test-classes.hh"
#include <iostream>

#include "text.hh"

namespace
{
using namespace exercise;
using namespace exerciseTest;

using exercise::Text;
using trompeloeil::_;

#ifdef HAS_GROUP_MOVE_CTOR
#if 0 // these checks don't work for now (Shape is ABC and not constructible)
static_assert(std::is_copy_constructible_v<Shape>, "Shape must be copy constructible");

static_assert(std::is_move_constructible_v<Shape>, "Shape must be move constructible");
#endif
#ifdef HAS_GROUP_MOVE_ASSIGNMENT
static_assert(std::is_copy_assignable_v<Shape>, "Shape must be copy assignable");
static_assert(std::is_move_assignable_v<Shape>, "Shape must be move assignable");
#endif

struct CopyMoveShape : Shape
{
    using Shape::Shape;
    void doDraw(cairo_t *) const override{}
};
static_assert(std::is_copy_constructible_v<CopyMoveShape>, "Shape must be copy constructible");
static_assert(std::is_move_constructible_v<CopyMoveShape>, "Shape must be move constructible");
#endif

const lest::test generalTests[] =
{
    {CASE("Shape position correctly initialized")
     {
         TestShape t1;
         TestShape t2("T2", Position(250, -36.5), Color(15.2, -66, 0), Pen(5E77));
         EXPECT(t1.getPosition().x == 0);
         EXPECT(t2.getPosition().y == -36.5);
     }},
    {CASE("Test Text::setName w/lvalue")
     {
         int allocCnt1, allocCnt2, allocCnt3;
         std::string s1{"some long string that hopefully is too long for small string optimization"};
         std::string s2;
         Text testObj{s2, s2};
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management
             {
                 ALLOW_CALL(tracer, trackNew(_, _));
                 ALLOW_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 allocCnt1 = testAlloc.getCount();
                 s2 = s1;
                 allocCnt2 = testAlloc.getCount() - allocCnt1;
                 testObj.setText(s1);
                 allocCnt3 = testAlloc.getCount() - (allocCnt2 + allocCnt1);
                 EXPECT(allocCnt3 == allocCnt2);
             }
             EXPECT(testOk());
             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
     }},
#ifdef HAS_TEXT_FORWARD_SET_NAME
    {CASE("Test Text::setName w/rvalue")
     {
         int allocCnt1, allocCnt2;
         std::string s1{"some long string that hopefully is too long for small string optimization"};
         std::string s2;
         Text testObj{s2, s2};
         AllocTracerMock tracer;
         AllocMock testAlloc;
         { // we need another scope for memory management
             {
                 ALLOW_CALL(tracer, trackNew(_, _));
                 ALLOW_CALL(tracer, trackDelete(_));

                 TraceSetter ts{testAlloc, &tracer};

                 allocCnt1 = testAlloc.getCount();
                 testObj.setText(std::move(s1));
                 allocCnt2 = testAlloc.getCount();
                 EXPECT(allocCnt1 == allocCnt2);
             }
             EXPECT(testOk());
             // now all acquired resources should be released
         }
         EXPECT(testAlloc.ok());
     }},
#endif
};
} // unnamed namespace

int main(int argc, char *argv[])
{
    exerciseTest::setTromploeilReporter();

    return lest::run(generalTests, argc, argv, std::cerr);
}
