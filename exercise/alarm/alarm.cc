// simple alarm program

/*
 * Copyright (c) 2004-2021 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <iostream>
#include <thread>
#include <string>
#include <chrono>
#include <cstdlib>

using std::string;
using std::getline;
using std::endl;
using std::cout;
using std::cin;
using std::flush;


class Alarm
{
public:
    Alarm(unsigned int seconds, std::string const &msg)
        : duration(seconds)
        , message(msg)
    {
    }

    void sleep() const
    {
	std::this_thread::sleep_for(duration);

        cout << "ALARM!!! " << message << endl;
    }

private:
    std::chrono::seconds duration;
    std::string message;
};


int main()
{
    const char *emptyMsg{""};
    string line;

    while (true)
    {
        cout << "Alarm> " << flush;
        if (!getline(cin, line)) break;

        char *rest;
        unsigned int relTime = strtoul(line.c_str(), &rest, 10);
        if (relTime == 0) break;
        const char *msg{*rest ? rest : emptyMsg};

        Alarm a{relTime, msg};
        a.sleep();
    }

    return 0;
}
