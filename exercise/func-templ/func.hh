#ifndef FUNC_TEMPL_HH_SEEN
#define FUNC_TEMPL_HH_SEEN
#include <iostream>
#include <utility>
#include <vector>

namespace exercise
{
inline bool readInt(std::istream &in, std::ostream &out,
                    int &trgt, int bound1, int bound2)
{
    if (bound1 > bound2)
    {
        std::swap(bound1, bound2);
    }

    int i = 0;
    bool prompt = false;
    do
    {
        if (prompt)
        {
            out << "outside bounds, please try again" << std::endl;
        }
        prompt = true;
        in >> i;
        if (!in)
        {
            in.clear();
            return false;
        }
    } while (i < bound1 || i > bound2);

    trgt = i;
    return true;
}

inline void printVector(std::ostream &o, std::vector<int> const &v)
{
    int idx = 0;

    for (int i: v)
    {
        o << idx << ": " << i << '\n';
        ++idx;
    }
}
} // namespace exercise
#endif /* FUNC_TEMPL_HH_SEEN */
