/*
 * Copyright (c) 2014-2020 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#if __has_include("func.hh")
#include "func.hh"
#endif

#include <iostream>
#include <vector>

namespace exercise
{
// readInt()
// printVector()
}

//#define HAVE_READ_TEMPL 1
//#define HAVE_PRINT_TEMPL 1
#include "testimpl.hh"

using std::cin;
using std::clog;
using std::cout;
using std::vector;

using namespace exercise;

int main()
{
    exerciseTest::runTests();

    vector<int> vi;

    // read into vector
    bool ok = true;
    while (ok)
    {
        int i;
        cout << "Please enter a number between 1 and 1000, press '.' to stop: ";
        ok = readInt(cin, clog, i, 1, 1000);

        if (ok)
        {
            vi.push_back(i);
        }
    }
    // print
    cout << '\n';
    printVector(cout, vi);

#if defined(HAVE_PRINT_TEMPL) && 0
    cin.clear();
    std::string dummy;
    std::getline(cin, dummy);
    int i = 22;
    cout << "Please enter a number between 0 and 7.5: ";
    readVal(cin, clog, i, 0.0, 7.5f);
    cout << i << '\n';
#endif

    return 0;
}
