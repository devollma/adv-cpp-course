/*
 * Copyright (c) 2018-2020 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#pragma once
#ifndef LOG_HELPER_HH_SEEN
#define LOG_HELPER_HH_SEEN
#include <iostream>
#include <vector>
#include <cstdint>
#include <type_traits>
#include <typeinfo>
#include <string_view>

#ifndef USE_LOG
#define USE_LOG 0
#endif

#undef LOG
#undef LVAR
#undef LHEX
#undef FLOG
#undef FLVAR

inline char logHexHelper(std::ostream &o, void const *p, size_t len)
{
    static char hexDigit[17] = "0123456789abcdef";
    uint8_t const *data = reinterpret_cast<uint8_t const *>(p);
    for (size_t i = 0; i != len; ++i, ++data)
    {
        o << hexDigit[(*data) >> 4] << hexDigit[(*data) & 0xf];
    }
    return ' ';
}

#if (USE_LOG)
struct LogEntry
{
    int line;
    uint64_t data;
};
typedef std::vector<LogEntry> LogQueue;
extern LogQueue globalLog;

struct CtorLogger
{
    CtorLogger(std::string_view file
               , int line
               , std::string_view func)
    {
        std::cerr << file << ':' << line << ':' << func << ": ctor log\n";
    }
};

inline void printLogQueue(std::ostream &o)
{
    for (LogEntry l: globalLog)
    {
        o << l.line << ": " << l.data << '\n';
        globalLog.clear();
    }
}

template <typename T>
constexpr auto printable(T const &x)
{
    if constexpr (sizeof(T) == 1)
        return int(x);
    else if constexpr (std::is_enum_v<T>)
        return int(x);
    else
    {
        return x;
    }
}

#define LOG (std::cerr << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << ": log\n")
#define LVAR(x) (std::cerr << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << ": " << #x << ':' << printable(x) << '\n')
#define LTVAR(msg, x) (std::cerr << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << ": " << msg << ':' << (x) << '\n')
#define LHEX(p, l) (std::cerr << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << ": " << #p << ':' << logHexHelper(std::cerr, p, l) << '\n')

#define FLVAR(x) (globalLog.push_back({__LINE__, x}))
#define FLOG (FLVAR(0))

#define LOGVARNAME(v, l) v ## l
#define CTOR_LOG_MEM(f, l, fun) CtorLogger LOGVARNAME(tmp_, l) = {f, l, fun}
#define CTOR_LOG CTOR_LOG_MEM(__FILE__, __LINE__, __FUNCTION__)

#else
#define LOG ((void)0)
#define LVAR(x) ((void)0)
#define LTVAR(msg, x) ((void)0)
#define LHEX(p, l) ((void)0)
#define FLOG ((void)0)
#define FLVAR(x) ((void)0)
#define CTOR_LOG /* no error on stray ';' in class member list */
#endif

#endif /* LOG_HELPER_HH_SEEN */
