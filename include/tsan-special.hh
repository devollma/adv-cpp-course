#ifdef TSAN_ENABLED
#define TSAN_FLUSH AnnotateFlushState(__FILE__, __LINE__)
extern "C" void AnnotateFlushState(const char* f, int l);
#else
#define TSAN_FLUSH
#endif
