// queue implementation
#ifndef QUEUE_INCLUDED_HH
#define QUEUE_INCLUDED_HH

/*
 * Copyright (c) 2004-2021 Detlef Vollmann, vollmann engineering gmbh
 * 
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <mutex>
#include <condition_variable>
#include <list>

enum class queue_op_status  { closed, ok };

template <class T> class MsgQueue
{
    typedef std::unique_lock<std::mutex> Lock;
public:
    MsgQueue()
      : finished{false} {}

    void wait_push(T const &item)
    {
        // no need to wait as we don't have a bounded queue
        Lock l{mtx};
        store.push_back(item);
        l.unlock();

        notEmpty.notify_one();
    }
    queue_op_status wait_pop(T &ret)
    {
        Lock l{mtx};

        while (store.empty() && !finished) notEmpty.wait(l);

        if (!store.empty())
        {
            ret = store.front();
            store.pop_front();

            return queue_op_status::ok;
        }
        else
        {
            return queue_op_status::closed;
        }

        // no need to notify as nobody waits for it
    }

    void close()
    {
        Lock l{mtx};

        finished = true;

        l.unlock();
        notEmpty.notify_all();
    }

private:
    std::list<T> store;
    mutable std::mutex mtx;
    mutable std::condition_variable notEmpty;
    bool finished;
};
#endif /* QUEUE_INCLUDED_HH */
