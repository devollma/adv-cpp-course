#pragma once
#ifndef QUEUE_HH_INCLUDES
#define QUEUE_HH_INCLUDES

#include <mutex>
#include <condition_variable>
#include <utility>
#include <memory>

namespace own
{
// from p0260r2 (2017-10):
// slightly modified

enum class queue_op_status
{
    success = 0,
    empty,
    full,
    closed
};

template <typename Value>
class buffer_queue
{
    buffer_queue() = delete;
    buffer_queue(const buffer_queue&) = delete;
    buffer_queue& operator =(const buffer_queue&) = delete;
    typedef std::unique_lock<std::mutex> Lock;

public:
    typedef Value value_type;

    explicit buffer_queue(size_t max_elems)
      : buf{new Value[max_elems]}
      , maxCnt{max_elems}
      , cnt{0}
      , head{0}
      , tail{0}
      , closed{false}
    {}
    template <typename Iter>
    buffer_queue(size_t max_elems, Iter first, Iter last);
    ~buffer_queue() noexcept = default;

    void close() noexcept
    {
        Lock l{mtx};
        closed = true;

        l.unlock();
        notFull.notify_all();
    }
    bool is_closed() const noexcept;
    bool is_empty() const noexcept;
    bool is_full() const noexcept;
    static bool is_lock_free() noexcept;

    Value value_pop();
    queue_op_status wait_pop(Value &v)
    {
        queue_op_status ret = queue_op_status::success;
        Lock l{mtx};

        while (cnt == 0 && !closed) notEmpty.wait(l);

        if (cnt != 0)
        {
            v = std::move(buf[head]);
            --cnt;
            head = (head+1) % maxCnt;
        }
        else
        {
            ret = queue_op_status::closed;
        }

        l.unlock();
        notFull.notify_one();

        return ret;
    }
    queue_op_status try_pop(Value&);

    void push(const Value& v);
    queue_op_status wait_push(const Value& v)
    {
        Lock l{mtx};

        if (closed) return queue_op_status::closed;

        while (cnt == maxCnt) notFull.wait(l);

        buf[tail] = v;
        ++cnt;
        tail = (tail+1) % maxCnt;

        l.unlock();
        notEmpty.notify_one();

        return queue_op_status::success;
    }
    queue_op_status try_push(const Value& v);
    void push(Value&& x);
    queue_op_status wait_push(Value&& x);
    queue_op_status try_push(Value&& x);

private:
    std::unique_ptr<Value[]> buf;
    mutable std::mutex mtx;
    mutable std::condition_variable notEmpty;
    mutable std::condition_variable notFull;
    size_t maxCnt;
    size_t cnt;
    size_t head;
    size_t tail;
    bool closed;
};

template <typename Value>
class QHandle
{
public:
    typedef Value value_type;

    QHandle() = default;
    explicit QHandle(size_t max_elems)
      : p{new buffer_queue<Value>{max_elems}}
    {}

    void close() noexcept
    {
        p->close();
    }
    queue_op_status wait_pop(Value &v)
    {
        return p->wait_pop(v);
    }
    queue_op_status wait_push(const Value& v)
    {
        return p->wait_push(v);
    }
    queue_op_status wait_push(Value&& v)
    {
        return p->wait_push(move(v));
    }

private:
    std::unique_ptr<buffer_queue<Value>> p;
};

template <typename T>
class queue_back
{
public:
    void push(T);
};
} // namespace own
//#undef LOG
#endif /* QUEUE_HH_INCLUDES */
