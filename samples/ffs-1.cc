// $Id$
// finding words in files, try #3

/*
 * Copyright (c) 2004-2011 Detlef Vollmann, vollmann engineering gmbh
 * All rights reserved.
 * 
 * Permission to use, copy, modify, distribute and sell this software
 * for any purpose is hereby granted without fee, provided that the
 * above copyright notice appears in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  Detlef Vollmann and vollmann engineering gmbh make
 * no representations about the suitability of this software for any
 * purpose. It is provided "as is" without express or implied warranty.
 */


#include "mtqueue.hpp"
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <thread>
#include <string>
#include <exception>
#include <system_error>

using std::string;
using std::getline;
using std::endl;
using std::exception;
using std::system_error;
using std::cerr;
using std::cout;

using std::thread;


namespace fs=boost::filesystem;


typedef MsgQueue<fs::path> FileList;

FileList *files;
std::string sWord;

void searchWord()
{
    try
    {
        fs::path fname;

        while(files->get(fname))
        {
            //cout << "searching " << fname << endl;
            fs::ifstream f(fname);

            string line;
            while (getline(f, line))
            {
                if (line.find(sWord) != line.npos)
                {
                    cout << "found in " << fname.string() << endl;
                    break;
                }
            }
        }
    }
    catch (system_error &err) { cerr << err.code().message() << endl; }
    catch (exception &err) { cerr << err.what() << endl; }
}

class SearchFiles
{
public:
    SearchFiles(string const &directory) : dir(directory) {}

    void operator()();

private:
    fs::path dir;
};

void SearchFiles::operator()()
{
    try
    {
        if (fs::exists(dir) && fs::is_directory(dir))
        {
            fs::directory_iterator end;
            for (fs::directory_iterator i(dir); i != end; ++i)
            {
                if (!fs::is_directory(*i))
                {
                    files->insert(*i);
                    //cout << "pushed " << *i << endl;
                }
            }
        }
        else
        {
            cerr << "no such directory" << endl;
        }
	files->finish();
    }
    catch (system_error &err) { cerr << err.code().message() << endl; }
    catch (exception &err) { cerr << err.what() << endl; }
}



int
main(int argc, char *argv[])
{
    try
    {
        sWord = argv[1];
        files = new FileList;

        SearchFiles sf(argv[2]);
        thread a(sf);
        thread b1(searchWord);
        thread b2(searchWord);
        thread b3(searchWord);
        thread b4(searchWord);
        thread b5(searchWord);

        a.join();
        b1.join();
        b2.join();
        b3.join();
        b4.join();
        b5.join();

        delete files;
    }
    catch (system_error &err) { cerr << err.code().message() << endl; }
    catch (exception &err) { cerr << err.what() << endl; }
	
	return 0;
}
