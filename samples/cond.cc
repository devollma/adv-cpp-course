// $Id$
/* sample program demonstrating a condition variable */

/*
 * Copyright (c) 2004-2011 Detlef Vollmann, vollmann engineering gmbh
 * All rights reserved.
 * 
 * Permission to use, copy, modify, distribute and sell this software
 * for any purpose is hereby granted without fee, provided that the
 * above copyright notice appears in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  Detlef Vollmann and vollmann engineering gmbh make
 * no representations about the suitability of this software for any
 * purpose. It is provided "as is" without express or implied warranty.
 */

#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <exception>
#include <system_error>
#include <string>
#include <unistd.h>

using std::string;
using std::endl;
using std::exception;
using std::system_error;
using std::cerr;
using std::cout;

using std::thread;
using std::condition_variable;
using std::mutex;
using std::unique_lock;

string data;
mutex dataMtx;
condition_variable dataThere;

void provideData()
{
    sleep(2);

    unique_lock<mutex> lock(dataMtx);
    data = "That's it";

    lock.unlock();
    dataThere.notify_one();
}

void useData()
{
    unique_lock<mutex> lock(dataMtx);

    while (data.empty()) dataThere.wait(lock);
    //dataThere.wait(lock, [this] { return !data.empty(); }

    cout << data << endl;
}


int main()
{
    try
    {
        thread use(&useData);
        thread provide(&provideData);

        use.join();
        provide.join();
    }
    catch (system_error &err) { cerr << err.code().message() << endl; }
    catch (std::exception &err) { std::cerr << err.what() << std::endl; }

    return 0;
}
