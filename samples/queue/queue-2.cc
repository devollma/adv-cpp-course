// queue implementation, try #2
// doesn't work correctly, DON'T USE!!!

/*
 * Copyright (c) 2004-2016 Detlef Vollmann, vollmann engineering gmbh
 * All rights reserved.
 * 
 * Permission to use, copy, modify, distribute and sell this software
 * for any purpose is hereby granted without fee, provided that the
 * above copyright notice appears in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  Detlef Vollmann and vollmann engineering gmbh make
 * no representations about the suitability of this software for any
 * purpose. It is provided "as is" without express or implied warranty.
 */

#include <mutex>
#include <condition_variable>
#include <list>

#include <string>
#include <future>
#include <iostream>

template <class T> class MsgQueue
{
    typedef std::unique_lock<std::mutex> Lock;
public:
    MsgQueue() = default;

    void insert(T const &item)
    {
        Lock l(mtx);
        store.push_back(item);
        //l.unlock();
        notEmpty.notify_one();
    }
    void get(T &ret)
    {
        Lock l(mtx);

        while (store.empty()) notEmpty.wait(l);

        ret = store.front();
        store.pop_front();
    }

private:
    std::list<T> store;
    mutable std::mutex mtx;
    mutable std::condition_variable notEmpty;
};

typedef MsgQueue<std::string> SList;

void fill(SList *q)
{
    q->insert("Hello");
    q->insert("parallel");
    q->insert("concurrent");
    q->insert("future");
    q->insert("C++");
    q->insert("World");
}

void search(SList *src, SList *result, char c)
{
    std::string s;
    while (true)
    {
        src->get(s);
        if (s.find(c) != std::string::npos)
        {
            result->insert(s);
        }
    }
}

int main()
{
    SList src, result;
    auto f = std::async(fill, &src);
    auto s1 = std::async(search, &src, &result, 'e');
    auto s2 = std::async(search, &src, &result, 'e');

    f.get();
    s1.get();
    s2.get();

    std::string s;
    while (true)
    {
        result.get(s);
        std::cout << s << '\n';
    }

    return 0;
}
