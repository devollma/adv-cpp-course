// queue implementation, try #1
// doesn't work correctly, DON'T USE!!!

/*
 * Copyright (c) 2004-2016 Detlef Vollmann, vollmann engineering gmbh
 * All rights reserved.
 * 
 * Permission to use, copy, modify, distribute and sell this software
 * for any purpose is hereby granted without fee, provided that the
 * above copyright notice appears in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  Detlef Vollmann and vollmann engineering gmbh make
 * no representations about the suitability of this software for any
 * purpose. It is provided "as is" without express or implied warranty.
 */

#include <mutex>
#include <condition_variable>
#include <list>

#include <string>
#include <future>
#include <iostream>

template <class T> class LQueue
{
    typedef std::unique_lock<std::mutex> Lock;
public:
    LQueue() = default;

    void insert(T const &item)
    {
        Lock l(mtx);
        store.push_back(item);
    }
    T remove()
    {
        Lock l(mtx);
        T tmp(store.front());
        store.pop_front();
        return tmp;
    }

    T front() const { return store.front(); }
    T back() const { return store.back(); }
    unsigned int empty() const { return store.empty(); }
    unsigned int size() const { return store.size(); }
    void erase(void)
    {
        Lock l(mtx);
        store.clear();
    }
    void wait() const
    {
        Lock l(mtx);
        notEmpty.wait(l);
    }
    void notify() { notEmpty.notify_one(); }

private:
    std::list<T> store;
    mutable std::mutex mtx;
    mutable std::condition_variable notEmpty;

};

typedef LQueue<std::string> SList;

void fill(SList *q)
{
    q->insert("Hello");
    q->insert("parallel");
    q->insert("concurrent");
    q->insert("future");
    q->insert("C++");
    q->insert("World");
}

void search(SList *src, SList *result, char c)
{
    while (!src->empty())
    {
        src->wait();
        std::string s{src->remove()};
        if (s.find(c) != std::string::npos)
        {
            result->insert(s);
        }
    }
}

int main()
{
    SList src, result;
    //auto f = std::async(std::launch::async, fill, src);
    auto f = std::async(fill, &src);
    auto s1 = std::async(std::launch::async, search, &src, &result, 'e');
    auto s2 = std::async(std::launch::async, search, &src, &result, 'e');

    f.get();
    s1.get();
    s2.get();

    while (!result.empty())
    {
        std::cout << result.remove() << '\n';
    }

    return 0;
}
