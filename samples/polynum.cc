#include <iostream>
#include <type_traits>
#include <string>

#define WITH_CONCEPTS 0

using std::ostream;
using std::cout;

struct NumIF;
template <typename T> struct ConcreteNum;

struct PolyNumber {
    template <typename T>
#if WITH_CONCEPTS
        requires std::is_convertible_v<T, double>
        ||  std::is_convertible_v<T, long long>
        ||  std::is_convertible_v<T, unsigned long long>
#endif
    PolyNumber(T);
    template <typename T> PolyNumber &operator=(T);

    template <typename T> operator ConcreteNum<T>() const;
    PolyNumber add(PolyNumber const &other) const;
    ostream &print(ostream &) const;

  private:
    NumIF *obj;
};

PolyNumber operator+(PolyNumber const &a, PolyNumber const &b)
{
    return a.add(b);
}

ostream &operator<<(ostream &o, PolyNumber const &num)
{
    return num.print(o);
}

struct NumIF
{
    virtual ~NumIF() = default;
    virtual PolyNumber add(PolyNumber const &) const = 0;
    virtual ostream &print(ostream &) const = 0;
    template <typename T> T value() const
    {
        if constexpr (std::is_signed_v<T>)
        {
            return getLongLong();
        } else if constexpr (std::is_integral_v<T>)
        {
            return getULongLong();
        } else {
            return getDouble();
        }
    }

protected:
    virtual double getDouble() const = 0;
    virtual long long getLongLong() const = 0;
    virtual unsigned long long getULongLong() const = 0;
};

template <typename T>
struct ConcreteNum : public NumIF
{
    ConcreteNum() = default;
    ConcreteNum(T t)
      : num{t}
    {
    }

    PolyNumber add(PolyNumber const &other) const override
    {
        return num + ConcreteNum(other).num;
    }

    ostream &print(ostream &o) const override
    {
        return o << num;
    }

protected:
    virtual double getDouble() const override
    {
        return double(num);
    }
    virtual long long getLongLong() const override
    {
        return (long long)(num);
    }
    virtual unsigned long long getULongLong() const override
    {
        return (unsigned long long)(num);
    }

private:
    T num = 0;
};

template <typename T>
#if WITH_CONCEPTS
        requires std::is_convertible_v<T, double>
        ||  std::is_convertible_v<T, long long>
        ||  std::is_convertible_v<T, unsigned long long>
#endif
PolyNumber::PolyNumber(T t)
    : obj{new ConcreteNum<T>{t}}
{
}

template <typename T> PolyNumber &PolyNumber::operator=(T t)
{
    delete obj;
    obj = new ConcreteNum<T>{t};

    return *this;
};

template <typename T> PolyNumber::operator ConcreteNum<T>() const
{
    return {obj->value<T>()};
}

PolyNumber PolyNumber::add(PolyNumber const &other) const
{
    return obj->add(other);
}

ostream &PolyNumber::print(ostream &o) const
{
    return obj->print(o);
}

int main()
{
    PolyNumber pi{5};
    PolyNumber pd{5.7};

    cout << pi + pd << '\n';

    std::string s;
    //PolyNumber ps{s};

    return 0;
}
