// $Id$
// finding words in files, queue implementation, try #3

/*
 * Copyright (c) 2004-2011 Detlef Vollmann, vollmann engineering gmbh
 * All rights reserved.
 * 
 * Permission to use, copy, modify, distribute and sell this software
 * for any purpose is hereby granted without fee, provided that the
 * above copyright notice appears in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  Detlef Vollmann and vollmann engineering gmbh make
 * no representations about the suitability of this software for any
 * purpose. It is provided "as is" without express or implied warranty.
 */

#ifndef CPP_MT_MTQUEUE_HPP
#define CPP_MT_MTQUEUE_HPP

#include <mutex>
#include <condition_variable>
#include <list>



template <class T> class MsgQueue
{
    typedef std::unique_lock<std::mutex> Lock;
public:
	MsgQueue() : finished(false) {}

	void insert(T const &item)
    {
        Lock l(mtx);
        store.push_back(item);

        notEmpty.notify_one();
    }
	bool get(T &ret)
    {
        Lock l(mtx);

        while (store.empty() && !finished) notEmpty.wait(l);

        if (!store.empty())
        {
            ret = store.front();
            store.pop_front();

            return true;
        }
        else
        {
            return false;
        }
    }

    void finish()
    {
        Lock l(mtx);

        finished = true;
        notEmpty.notify_all();
    }

private:
	std::list<T> store;
	mutable std::mutex mtx;
	mutable std::condition_variable notEmpty;
    bool finished;
};

#endif // CPP_MT_MTQUEUE_HPP
