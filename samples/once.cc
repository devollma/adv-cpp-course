// $Id$
/* call_once demo */

/*
 * Copyright (c) 2012 Detlef Vollmann, vollmann engineering gmbh
 * All rights reserved.
 * 
 * Permission to use, copy, modify, distribute and sell this software
 * for any purpose is hereby granted without fee, provided that the
 * above copyright notice appears in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  Detlef Vollmann and vollmann engineering gmbh make
 * no representations about the suitability of this software for any
 * purpose. It is provided "as is" without express or implied warranty.
 */

#include <mutex>

struct SomeClass
{
    int i;
};

SomeClass* globalObject() {
    static std::once_flag flag;
    static SomeClass *p = nullptr;
    std::call_once(flag, []{ p = new SomeClass(); });

    return p;
}

int main()
{
    globalObject();

    return 0;
}
