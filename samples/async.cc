// $Id$
/* async example */

/*
 * Copyright (c) 2012 Detlef Vollmann, vollmann engineering gmbh
 * All rights reserved.
 * 
 * Permission to use, copy, modify, distribute and sell this software
 * for any purpose is hereby granted without fee, provided that the
 * above copyright notice appears in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  Detlef Vollmann and vollmann engineering gmbh make
 * no representations about the suitability of this software for any
 * purpose. It is provided "as is" without express or implied warranty.
 */

#include <future>
#include <iostream>

double computePi()
{
    // some long computation

    return 3.14159;
}

int main()
{
    std::future<double> f = std::async(computePi);

    // do other work...
    double myPi = f.get();

    std::cout << myPi << '\n';

    return 0;
}
