// testing specialization on iterator tag

#include <iostream>
#include <iterator>
#include <vector>
#include <list>

using std::cout;

// generic implementation
template <class TagType>
class LenImpl
{
public:
    template <class It>
    size_t operator()(It start, It end)
    {
        cout << "generic algo\n";
        int l = 0;
        while (start != end)
        {
            ++l;
            ++start;
        }
        return l;
    }
};

// implementation for random access iterator
template <>
class LenImpl<std::random_access_iterator_tag>
{
public:
    template <class It>
    size_t operator()(It start, It end)
    {
        cout << "algo for random access\n";
        return end - start;
    }
};

template <class It>
size_t length(It start, It end)
{
    return LenImpl<typename std::iterator_traits<It>
                              ::iterator_category>()(start, end);
}


int main()
{
    std::list<int> l = { 1, 2, 3 };
    std::vector<double> v(5, 3.14);

    cout << length(l.begin(), l.end()) << '\n';
    cout << length(v.begin(), v.end()) << '\n';

    return 0;
}
