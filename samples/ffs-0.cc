
void threadA()
{
    DirIterator d = 
        DirBeginSearch(..., "*.txt");
    while (d) {
	fileName = *d;
	++d;
	fileList.push(fileName);
	++fileCount;
    }
}

void threadB()
{
    while (fileCount > 0) {
	fileList.pop(fileName);
	--fileCount;
	search(fileName,
	       searchString);
    }
}
