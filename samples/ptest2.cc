// ptest2.cc
// test for pipeline w/ task specific executor
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <array>

#include "pipeline.hh"
#include "logging.hh"

#include <future>
#include <experimental/thread_pool>

//#define LOG (cerr << __LINE__ << '\n')

using namespace std;
using namespace std::experimental;
using namespace own;
using namespace test;

template <size_t maxCnt>
ValueWState<int> genNums()
{
    static size_t cnt = 0;
    static const size_t numCnt = 8;
    static std::array<int, numCnt> nums{ 22, 44, 64, 400, 9, 16, 1024, 625 };

    if (cnt == maxCnt)
    {
        return {ReturnState::finished};
    } else {
        //return {rand()};
        cnt = cnt % numCnt;
        return {nums[cnt++]};
    }
}

auto sqroot = [] (int n) -> ValueWState<float>
{
    return sqrtf(float(n));
};

template<class Executor>
logging_executor<Executor>
require(Executor ex, decltype(sqroot) const &)
{
    return logging_executor<Executor>("sqr", ex);
}


template <typename T>
void out(T val)
{
    cout << val << endl;
}

int main()
{
    static_thread_pool pool{5};

    (makeFrontPipe(genNums<5>)
     | sqroot
     | out).run(pool.executor());

    return 0;
}
