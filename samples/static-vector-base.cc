/*
 * Copyright (c) 2014-2021 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <string>
#include <memory>
#include <functional>
#include <cstdint>
#include <map>
#include <iostream>

// base variant of StaticVector
template <class T>
class StaticVector
{
public:
    explicit StaticVector(size_t size);
    ~StaticVector();

    T *begin();
    T *end();

    template <typename U> T &push_back(U &&t);
    void pop();

    T &operator[](int idx);

    size_t size() const;

private:
    size_t dataSize;
    size_t realSize = 0;

    std::byte *data;

    T *ptr(size_t idx);
};

template <typename T>
StaticVector<T>::StaticVector(size_t size)
  : dataSize{size}
  , data{new std::byte[sizeof(T) * size]}
{
}

template <typename T>
StaticVector<T>::~StaticVector()
{
        for (size_t i = 0; i != realSize; ++i)
        {
            ptr(i)->~T();
        }
        // free memory
        delete [] data;
}

template <typename T>
T *StaticVector<T>::begin()
{
    return reinterpret_cast<T *>(data);
}

template <typename T>
T *StaticVector<T>::end()
{
    return ptr(realSize);
}

template <typename T>
  template <typename U>
T &StaticVector<T>::push_back(U &&t)
{
    // no checks
    new (ptr(realSize)) T(std::forward<U>(t));
    return *(ptr(realSize++));
}

template <typename T>
void StaticVector<T>::pop()
{
    ptr(--realSize)->~T();
}

template <typename T>
T &StaticVector<T>::operator[](int idx)
{
    return *(ptr(idx));
}

template <typename T>
size_t StaticVector<T>::size() const
{
    return realSize;
}

template <typename T>
T *StaticVector<T>::ptr(size_t idx)
{
    return reinterpret_cast<T *>(data + (idx * sizeof(T)));
}

struct TestInt
{
    TestInt(int i_) : i{i_} { std::cout << "TestInt(int): " << this << "\n"; }
    ~TestInt() { std::cout << "~TestInt(): " << this << "\n"; }

    operator int() const { return i; }

    int i;
};

int main()
{
    StaticVector<TestInt> sv{17};

    sv.push_back(5);
    std::cout << sv.size() << '\n';
    std::cout << sv[0] << '\n';
    sv.push_back(17);

    for (auto &i: sv)
    {
        std::cout << i.i << ' ';
    }
    std::cout << '\n';

    sv.pop();
    std::cout << sv.size() << '\n';

    return 0;
}
