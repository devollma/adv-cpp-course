#include "personnel.hh"
#include "heatingTable.hh"

#include <iostream>
#include <thread>
#include <cstdlib>
#include <vector>

using std::vector;
using std::thread;
using std::cerr;

const int heatingSize = 3;

class Restaurant {
public:
    Restaurant(int numDishes, int numChefs, int numWaiters)
      : table(heatingSize, numDishes)
    {
	waiters.reserve(numWaiters);
	for (int i = 0; i != numWaiters; ++i) {
	    waiters.push_back(thread(Waiter(&table, i)));
	}
	chefs.reserve(numChefs);
	for (int i = 0; i != numChefs; ++i) {
	    chefs.push_back(thread(Chef(&table, i)));
	}
    }
    ~Restaurant() {
	for (auto &c : chefs) {
	    c.join();
	}
	table.stop();
	for (auto &w : waiters) {
	    w.join();
	}
    }

private:
    HeatingTable table;
    vector<thread> waiters;
    vector<thread> chefs;
};

int main(int argc, char *argv[]) {
    if (argc != 4) {
	cerr << "usage: restaurant <#dishes> <#chefs> <#waiters>\n";
	return 1;
    }

    Restaurant rest(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));

    return 0;
}
