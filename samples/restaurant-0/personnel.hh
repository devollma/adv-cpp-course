class HeatingTable;

class Chef {
public:
    Chef(HeatingTable *shared, int ind);
    void operator()();
    int id() const { return ind; }

private:
    HeatingTable *sharedTable;
    int ind;
};

class Waiter {
public:
    Waiter(HeatingTable *shared, int ind);
    void operator()();
    int id() const { return ind; }

private:
    HeatingTable *sharedTable;
    int ind;
};
