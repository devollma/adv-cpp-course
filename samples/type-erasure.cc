// demonstration of virtual wrapper for templates

#include <memory>


class MatrixBase
{
public:
    virtual ~MatrixBase();

    virtual float computeDet() const;
    virtual float &value(int x, int y);
};

template <typename Storage, template<typename> class DetComp>
class MatrixTImpl : public MatrixBase
{
public:
    MatrixTImpl(int x, int y)
      : storeObj(x, y)
    {}

    float computeDet() const override
    {
        return detObj.compute(storeObj);
    }

    float &value(int x, int y) override
    {
        return storeObj.value(x, y);
    }

private:
    Storage storeObj;
    DetComp<Storage> detObj;
};

class MatrixVirt
{
public:
    template <typename Storage, template<typename> class DetComp>
    MatrixVirt(int x, int y, Storage *, DetComp<Storage>*)
      : handle(new MatrixTImpl<Storage, DetComp>(x, y)) {}
    MatrixVirt(MatrixBase *m)
      : handle(m)
    {}

    float computeDet() const
    {
        return handle->computeDet();
    }

    float &value(int x, int y)
    {
        return handle->value(x, y);
    }

private:
    std::unique_ptr<MatrixBase> handle;
};

template <typename Storage, template<typename> class DetComp>
MatrixBase *makeMatrix(int x, int y)
{
    return new MatrixTImpl<Storage, DetComp>(x, y);
}

class SparseStorage
{
public:
    SparseStorage(int x, int y);

    float &value(int x, int y);
};

template<typename Storage>
class SarrusRule
{
public:
    float compute(Storage const &) const;
};

template <typename Storage, template<typename> class DetComp>
class MatrixT
{
public:
    MatrixT(int x, int y)
      : storeObj(x, y)
    {}

    float computeDet() const
    {
        return detObj.compute(storeObj);
    }

    float &value(int x, int y)
    {
        return storeObj.value(x, y);
    }

private:
    Storage storeObj;
    DetComp<Storage> detObj;
};

void test()
{
    MatrixT<SparseStorage, SarrusRule> m1(50, 500000);

    MatrixVirt m2(makeMatrix<SparseStorage, SarrusRule>(275, 4285));

    MatrixVirt m3(100, 2000, (SparseStorage *)0, (SarrusRule<SparseStorage>*)0);

    //MatrixVirt m4 = MatrixVirt::create<SparseStorage, SarrusRule>(275, 4285);
}
