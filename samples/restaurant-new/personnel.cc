#include "personnel.hh"
#include "restaurant.hh"

#include <buffer_queue.h>

#include <chrono>
#include <thread>
#include <iostream>
#include <random>
#include <sstream>
#include <mutex>

using std::chrono::milliseconds;
using std::this_thread::sleep_for;
using std::cout;
using std::flush;
using std::cerr;
//using std::mutex;
//using std::lock_guard;
using std::ostringstream;
using std::ranlux24_base;
using std::uniform_int_distribution;

using gcl::latch;


namespace {
class SyncRand {
public:
    SyncRand()
      : dist(1, 8)
    {
    }
    uint32_t next() {
        std::lock_guard<std::mutex> l(mtx);
        return dist(rng);
    }

private:
    ranlux24_base rng;
    uniform_int_distribution<uint32_t> dist;
    std::mutex mtx;
};
SyncRand chefsRand;
SyncRand waitersRand;


void out(ostringstream &os) {
    os << "\n";
    cout << os.str() << flush;
    os.str("");
}
}


Chef::Chef(QueueType *shared, OrderDispenser *o,
           latch *finish, int index)
  : sharedTable(shared)
  , orders(o)
  , stopLatch(finish)
  , ind(index)
{
}

void Chef::operator()() {
    ostringstream s;
    int orderNo;

    while ((orderNo = orders->nextOrder())) {
        uint32_t sleepTime = 300 * chefsRand.next();
        out((s << "Chef number " << ind << " works for " << sleepTime << "ms on dish " << orderNo, s));
        sleep_for(milliseconds(sleepTime));
        sharedTable->push(orderNo);
    }

    stopLatch->count_down();
    out((s << "Chef number " << ind << " finished", s));
}

Waiter::Waiter(QueueType *shared, int index)
  : sharedTable(shared)
  , ind(index)
{
}

void Waiter::operator()() {
    ostringstream s;
    int orderNo;
    while ((sharedTable->wait_pop(orderNo) != gcl::queue_op_status::closed)) {
        uint32_t sleepTime = 200 * waitersRand.next();
        out((s << "Waiter number " << ind << " works for " << sleepTime << "ms", s));
        sleep_for(milliseconds(sleepTime));
    }

    out((s << "Waiter number " << ind << " finished", s));
}
