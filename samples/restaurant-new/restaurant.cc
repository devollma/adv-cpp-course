#include "restaurant.hh"
#include "personnel.hh"
//#include "heatingTable.hh"

#include <buffer_queue.h>
#include <latch.h>

#include <iostream>
#include <thread>
#include <cstdlib>
#include <vector>

using std::vector;
using std::thread;
using std::cerr;

using gcl::latch;

const int heatingSize = 3;

int OrderDispenser::nextOrder() {
    Locker l(mtx);

    if (val == max) {
        return 0;
    } else {
        ++val;
        return val;
    }
}

class Restaurant {
public:
    Restaurant(latch *cookingDone,
               int numDishes, int numChefs, int numWaiters)
      : table(heatingSize)
      , orders(numDishes)
      , chefFinish(cookingDone)
    {
        waiters.reserve(numWaiters);
        for (int i = 0; i != numWaiters; ++i) {
            waiters.push_back(thread(Waiter(&table, i)));
        }
        chefs.reserve(numChefs);
        for (int i = 0; i != numChefs; ++i) {
            chefs.push_back(thread(Chef(&table, &orders,
                                        chefFinish, i)));
        }
    }
    void stop() {
        table.close();
        for (auto &c : chefs) {
            c.join();
        }
        for (auto &w : waiters) {
            w.join();
        }
    }

private:
    QueueType table;
    OrderDispenser orders;
    latch *chefFinish;
    vector<thread> waiters;
    vector<thread> chefs;
};

int main(int argc, char *argv[]) {
    if (argc != 4) {
        cerr << "usage: restaurant <#dishes> <#chefs> <#waiters>\n";
        return 1;
    }
    int numChefs(atoi(argv[2]));
    latch chefsDone(numChefs);
    Restaurant rest(&chefsDone, atoi(argv[1]), numChefs, atoi(argv[3]));

    chefsDone.wait();
    rest.stop();

    return 0;
}
