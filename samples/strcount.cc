#include <algorithm>
#include <numeric>
#include <execution>

#include <unordered_map>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include "timeHelper.hh"

size_t wordMultiple = 5000;


class StringCount
{
    typedef std::unordered_map<std::string, int> MapT;
    typedef std::pair<std::string, int> PairT;
public:
    StringCount()
      : sRef{nullptr}
    {
    }
    StringCount(std::string const &s)
      : sRef{&s}
    {
    }

    void add(StringCount &other)
    {
        if (sRef)
        {
            counts[*sRef] = 1;
            sRef = nullptr;
        }

        if (other.sRef)
        {
            ++counts[*other.sRef];
        } else {
            for (auto const &p: other.counts)
            {
                counts[p.first] += p.second;
            }
        }
    }

    auto moveOut()
    {
        std::vector<PairT> rv;
        rv.reserve(counts.size());
        rv.assign(std::make_move_iterator(counts.begin()),
                  std::make_move_iterator(counts.end()));

        return rv;
    }

private:
    std::unordered_map<std::string, int> counts;
    std::string const *sRef;
};

typedef std::vector<std::string> WList;
namespace par = std::execution;

template <typename Policy>
void countWords(Policy pol, WList const &words)
{
    typedef std::shared_ptr<StringCount> SCPtr;
    Timer t;
    SCPtr init{std::make_shared<StringCount>()};
    auto rMap{std::transform_reduce(pol, words.begin(), words.end(),
                                    init,
                                    [] (SCPtr l, SCPtr r)
                                    {
                                        l->add(*r);
                                        return l;
                                    },
                                    [] (std::string const &s)
                                    {
                                        return std::make_shared<StringCount>(s);
                                    })};
    auto rVec = rMap->moveOut();
    std::sort(pol, rVec.begin(), rVec.end(),
              [] (auto const &l, auto const &r)
              {
                  return l.second > r.second;
              });
    int i = 0;
    for (auto const &p: rVec)
    {
        std::cout << p.first << ": " << p.second << '\n';
        if (i++ == 10) break;
    }
    std::cout << "Time(us): " << t << '\n';
}


int main()
{
    std::vector<std::string> words, fWords;

    std::ifstream in("carol11ms.txt", std::ios::in | std::ios::binary);
    while (in)
    {
        std::string s;
        in >> s;
        fWords.push_back(s);
    }
    words.reserve(wordMultiple*fWords.size());
    for (size_t i = 0; i != wordMultiple; ++i)
    {
        for (std::string const &s: fWords)
        {
            words.push_back(s);
        }
    }

    countWords(par::seq, words);
    countWords(par::par, words);

    return 0;
}
