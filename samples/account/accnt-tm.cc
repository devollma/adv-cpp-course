#include <cstdint>
#include <functional>

#include <deque>
#include <numeric>
#include <iostream>

class Account
{
public:
    Account(int64_t initVal)
      : val{initVal}
    {}

    void withdraw(int64_t amnt)
    {
        transaction([=] { val -= amnt; });
    }
    void deposit(int64_t amnt)
    {
        transaction([=] { val += amnt; });
    }
    int64_t balance()
    {
        int64_t v;
        transaction([this, &v] { v = val; });
        return v;
    }

private:

    void transaction(std::function<void()> f)
    {
        atomic_noexcept
        {
            f();
        }
    }

    int64_t val;
    //std::mutex mtx;
};

void transfer(Account &from, Account &to, int64_t amnt)
{
    atomic_noexcept
    {
        from.withdraw(amnt);
        to.deposit(amnt);
    }
}

void printAl(std::deque<Account> &al)
{
    for (size_t i{0}; i != al.size(); ++i)
    {
        std::cout << "a[" << i << "]: " << al[i].balance() << '\n';
    }
}

int main()
{
    std::deque<Account> acLst;
    acLst.emplace_back(2000);
    acLst.emplace_back(7500);
    acLst.emplace_back(2300);
    acLst.emplace_back(1200);
    printAl(acLst);

    transfer(acLst[1], acLst[2], 1500);
    printAl(acLst);

    atomic_noexcept
    {
        auto sum{std::accumulate(acLst.begin(), acLst.end(), 0LL,
                                 [] (int64_t s, Account &a)
                                 {
                                     return s + a.balance();
                                 })};
    }
    std::cout << "sum: " << sum << '\n';

    return 0;
}
