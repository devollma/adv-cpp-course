#include <cstdint>
#include <mutex>
#include <functional>
#include <iostream>

class Account
{
public:
    Account(int64_t initVal)
      : val{initVal}
    {}

    void withdraw(int64_t amnt)
    {
        transaction([this] { val -= amnt; });
    }
    void deposit(int64_t amnt)
    {
        transaction([this] { val += amnt; });
    }
    int64_t balance()
    {
        int64_t v;
        transaction([this, &v] { v = val; });
        return v;
    }

private:

    void transaction(std::function<void()> f)
    {
        std::lock_guard<std::mutex> l{mtx};
        f();
    }

    int64_t val;
    std::mutex mtx;
};

int main()
{
    Account a1{3000};
    Account a2{7000};
    std::cout << "a1: " << a1.balance() << ", a2: " << a2.balance() << '\n';

    int64_t trans{1500};

    a1.withdraw(trans);
    a2.deposit(trans);
    std::cout << "a1: " << a1.balance() << ", a2: " << a2.balance() << '\n';

    return 0;
}
